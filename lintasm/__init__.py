# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 09:11:13 2014

@author: catix
"""

__version__ = '0.1rc1'

##########################################################################
# IF YOU ADD A NEW FILE TO THE MODULE INCLUDE IT BY FILE NAME IN THIS LIST
__all__ = ["lintasm", "casmlst", "lintconfig", "helpers"]
##########################################################################