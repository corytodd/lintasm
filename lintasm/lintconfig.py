# -*- coding: utf-8 -*-
"""
Created on Fri Nov 28 09:06:56 2014

@author: catix
"""

import ConfigParser

CONFIG_FILE_NAME = 'lint.cfg'


def generate():
    """
    Generates a new config file for lintasm
    """
    config = ConfigParser.RawConfigParser()

    # TODO: Read existing config into memory so it can be restored
    # Be sure to capture any manually created sections/options

    # When adding sections or items, add them in the reverse order of
    # how you want them to be displayed in the actual file.
    # In addition, please note that using RawConfigParser's and the raw
    # mode of ConfigParser's respective set functions, you can assign
    # non-string values to keys internally, but will receive an error
    # when attempting to write to a file or when you get it in non-raw
    # mode. SafeConfigParser does not allow such assignments to take place.
    config.add_section('global')

    # Specify the default assembler mode, e.g. HEX (16), dec(10), octal(8)
    config.set('global', 'base', '16')
    # Report unused sections of code
    config.set('global', 'report_dead_code', 'true')

    # Options specific to CASM
    config.add_section('casm')

    # When set to true, lint will check for unsafe JSR calls. An unsafe JSR call
    # An unsafe call would be "jsr SOME_LABEL"
    config.set('casm', 'enforce_safe_jsr', 'true')

    # These commands support .b .w and .l size specifiers. It is considered best
    # practice to explicitly define the parameter width on these op codes.
    config.set('casm', 'size_req', ['move', 'clr', 'btst', 'bset', 'tst',
                                    'ori', 'cmpi', 'cmpa', 'andi', 'not',
                                    'adda'])

    # Writing our configuration file to 'example.cfg'
    with open(CONFIG_FILE_NAME, 'wb') as configfile:
        config.write(configfile)


def get_bool(section, option):
    """
    Attempt to return the given parameter from the specified config section
    """
    config = ConfigParser.RawConfigParser()
    config.read(CONFIG_FILE_NAME)

    result = config.getboolean(section, option)
    return result


def get(section, option):
    """
    Attempt to return the given parameter from the specified config section
    """
    config = ConfigParser.RawConfigParser()
    config.read(CONFIG_FILE_NAME)

    result = config.get(section, option)
    return result






