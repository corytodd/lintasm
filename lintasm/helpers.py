import re

def extract_hex(num):
    """
    Attempts to return the hex string of the given value.
    This will filter white space and return an empty string
    if the input is non-hex,  non-integer
    """
    temp = num

    if is_hex(num):
        # Eat all that whitespace
        temp = temp.replace(' ', '')

        # Eat any non-hex that may exist.
        temp = re.sub('[^A-Fa-f0-9]+', '', temp)
        return temp.strip()

    return ''


def extract_dec(num):
    """
    Attempts to return the decimal string of the given value.
    This will filter white space and return an empty string
    if the input is non-hex,  non-integer
    """
    temp = num

    if is_dec(num):
        # Eat all that whitespace
        temp = temp.replace(' ', '')

        # Eat any non-hex that may exist.
        temp = re.sub('[^0-9]+', '', temp)
        return temp.strip()

    return ''

def get_next_letter(line):
    """
    Returns the index of the next alphabetic character in this string.
    If none are found, return (False,-1) otherwise (True, value)
    """
    for i in xrange(len(line)):
        c = line[i]
        if c.isalpha() or (not c.isdigit() and c != ' '):
            return i-1
    return -1


def is_hex(string):
    """
    Returns true if string is value hexdecimal
    """
    try:
        int(string, 16)
        return True
    except ValueError:
        return False

def get_comment_start(text, comments = None):
    """
    Returns the index of the first comment character and the previous
    non-comment string. (int, string). If a comment character is not 
    found the index will be -1.

    Parameters:
    -----------
        text        --  string
        comments    --  list[] of characters (default: [';'])
    """
    if comments is None:
        comments = [';']
    
    non_comment = '';
    # Scan for the start of comment
    for i in xrange(len(text)):
        c = text[i]
        if c in comments:
            return i, non_comment
            break
        non_comment += c
    return -1, non_comment


def is_dec(string):
    """
    Returns true if string is valid base-10 number
    """
    try:
        int(string, 10)
        return True
    except ValueError:
        return False
