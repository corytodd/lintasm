# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 09:12:44 2014

@author: catix
"""

import sys
import getopt

from . import casmlst


def main(argv):
    """
    Main routine

    Keyword arguments:
    argv -- all command line arguments passed from interpreter
    """
    file_name = ''
    verbose = False
    usage = 'lintasm.py -f <list_file.lst> [options]'

    try:
        opts, args = getopt.getopt(argv, "hf:v", ["file=:verbose=:"])
    except getopt.GetoptError:
        print usage
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print usage
            sys.exit()

        elif opt in ("-v", "--verbose"):
            verbose = True

        elif opt in ("-f", "--file"):
            file_name = arg

    return parse(file_name, verbose)


# pylint: disable=R0912
# If you can make this less complex, please do so!
def parse(file_name, verbose=False):
    """
    Parses the given list file into a Casmlst object.

    Args:
        file_name -- string file name/path (relative or abs)
        verbose   -- show additional debugging information
    """

    if len(file_name) is 0:
        print 'Missing or empty listing file'

    else:
        if verbose:
            print 'Parsing list file:', file_name

        # 1st we read file into a massive array. Each element is a line of file
        with open(file_name) as input_file:
            content = input_file.readlines()

        # Here is what we expect. Remember This is zero indexed so column 1 in
        # your editor is [0] here.
        # Column    Value
        # 1         Hex Address
        # 10        MCU Op code/Raw data
        # 25        Line number END! e.g. 12<- cursor is after 12 on column 25
        # 26        Code labels
        # >27       Code,   comments,   data
        #
        # Note: Op codes may column wrap! They can be 4 bytes wide
        # (8 ASCII characters in editor)
        # and continue alone the same column. See L01TVN for example

        listfile_list = casmlst.ListFile(file_name)


        # We work one line at a time (lines are separated by CRLF)
        for line in content:

            # Eat the newline character
            line = line[:-1]

            # If the remaining line is empty -OR- the first character
            # is not a space, skip this line (assembler injects filename.ext at 1st column)
            if len(line) is 0 or line[0] is not ' ':
                continue

            # This gathers all the Hex addresses. If there is no address,
            # that means 1 of 2 things
            # 1) There is no code! Just comments or empty linker space
            # 2) The opCode wrapped its column. In this case,  no other data
            #    is on this line besides op code
            if line[1] is not ' ':

                # Awesome, add it to the list-mapper
                listfile_list.append(casmlst.ListFileEntry(line))

            # Catch those weird opcode column wraps
            elif len(listfile_list) > 0:

                # The opcode line may be alone or have a line number and text
                # Use len() to get the proper index
                if len(line) > 18:
                    listfile_list[-1].concat_op_code(line[10:18])
                elif len(line) > 10:
                    listfile_list[-1].concat_op_code(line[10:len(line) + 1])

        return listfile_list

# -------------------------------
if __name__ == "__main__":
    main(sys.argv[1:])
