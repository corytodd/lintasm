from distutils.core import setup

setup(
    name='lintasm',
    version='0.1',
    packages=['tests', 'lintasm'],
    url='https://bitbucket.org/corytodd/lintasm',
    license='BSD',
    author='catix',
    author_email='dev@lintasm.org',
    description='Coldfire assembler lint utility'
)
