import os
from unittest import TestCase
from lintasm import casmlst, lintasm

__author__ = 'catix'


GLOBAL_TEST_LST = 'test_list.lst'


class TestListFile(TestCase):
    def setUp(self):
        self.listfile = lintasm.parse(GLOBAL_TEST_LST)

    def test_compiled_size(self):
        self.assertEquals(35, self.listfile.compiled_size())

    def test_write(self):
        self.listfile.write()
        self.assertTrue(os.path.isfile(casmlst.DEFAULT_OUT_FILE))

    def test_append(self):
        entry = casmlst.ListFileEntry()
        entry.address = "00000001"
        entry.opcode = "8091"
        entry.index = "20"
        entry.label = "test"
        entry.text = "move.b    (aTEST,A5),D0   ;This is a test line"
        self.listfile.append(entry)
        self.assertEqual(entry, self.listfile[-1])