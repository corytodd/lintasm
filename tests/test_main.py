from unittest import TestCase
from lintasm import lintasm

__author__ = 'catix'

GLOBAL_TEST_LST = 'test_list.lst'


class TestListasm(TestCase):
    def test_parse(self):
        list_file = lintasm.parse('')
        self.assertIs(None, list_file, 'Nothing should be returned if empty/null string provided')

        list_file = lintasm.parse(GLOBAL_TEST_LST)
        self.assertIsNotNone(list_file, 'Parse result should not be empty')
        self.assertEqual(GLOBAL_TEST_LST, list_file.name, 'Parse result name should match source file parameter')

        list_file = lintasm.parse(GLOBAL_TEST_LST, True)
        self.assertIsNotNone(list_file, 'Parse result should not be empty')
        self.assertEqual(GLOBAL_TEST_LST, list_file.name, 'Parse result name should match source file parameter')

        self.assertEqual(str(list_file), 'Name: test_list.lst, 37 bytes, 14 lines of interest')

        print 'Testing small file for bad labels'
        bad_chars = ["'", "\"", ",", ".", " ", "$", "#", "!", ";"]
        exceptions = [".list", "$macro", "$BASE"]
        for entry in list_file:
            if any(bad in entry.label for bad in bad_chars):
                if not any(ex in entry.label for ex in exceptions):
                    print "Bad Entry: {:s}".format(entry)

        print 'Testing large file for bad labels'
        list_file = lintasm.parse('apex7000.lst')
        bad_chars = ["'", "\"", ",", ".", " ", "$", "#", "!", ";"]
        exceptions = [".list", "$macro", "$BASE", ".PAGELENGTH", ".PAGEWIDTH"]
        for entry in list_file:
            if any(bad in entry.label for bad in bad_chars):
                if not any(ex in entry.label for ex in exceptions):
                    print "WTF: {:s}".format(entry)

    def test_check_jsr(self):
        listfile = lintasm.parse(GLOBAL_TEST_LST)
        warnings = lintasm.check_jsr(listfile, True)
        print warnings
        self.assertEqual(1, len(warnings))

        warnings = lintasm.check_jsr(listfile, False)
        self.assertEqual(0, len(warnings))

        warnings = lintasm.check_jsr(listfile)
        self.assertEqual(1, len(warnings))

    def test_enforce_size_specifier(self):
        listfile = lintasm.parse(GLOBAL_TEST_LST)
        warnings = lintasm.enforce_size_specifier(listfile)
        self.assertEqual(1, len(warnings))

    def test_get_hot_routines(self):
        listfile = lintasm.parse(GLOBAL_TEST_LST)
        hot = lintasm.get_hot_routines(listfile, 1)
        self.assertEqual(hot, [('TEST', 2)])

    # def test_locate_dead_code(self):
    #     listfile = lintasm.parse(GLOBAL_TEST_LST)
    #     cold = lintasm.locate_dead_code(listfile)
    #     self.assertEqual(1, len(cold))