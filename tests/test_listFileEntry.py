from unittest import TestCase
from lintasm import casmlst

__author__ = 'catix'


class TestListFileEntry(TestCase):
    def test_concat_op_code(self):
        entry = casmlst.ListFileEntry()
        entry.opcode = "00000000"
        entry.concat_op_code("00")
        self.assertEqual('0000000000', entry.opcode)

    def test_get_code(self):
        entry = casmlst.ListFileEntry()
        entry.text = "move.b    (aTEST,A5),D0   ;This is a test line"
        self.assertEqual('move.b    (aTEST,A5),D0', entry.get_code())

        entry.text = ";move.b    (aTEST,A5),D0   ;This is a test line"
        self.assertEqual('', entry.get_code())

        entry.text = "move.b    (aTEST,A5),D0   "
        self.assertEqual('move.b    (aTEST,A5),D0', entry.get_code())