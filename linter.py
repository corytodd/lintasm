﻿__author__ = 'catix'

import datetime
import sys
import getopt

from lintasm import *

FULL_LOG_PATH = "lintroll.log"

def main(argv):
    """
    Main routine

    Keyword arguments:
    argv -- all command line arguments passed from interpreter
    """
    clean_log()
    file_name = ''
    verbose = False

    usage = 'linter.py -f <list_file.lst> [options]'
    clean_log()

    try:
        opts, args = getopt.getopt(argv, "hf:v", ["file=:verbose=:"])
    except getopt.GetoptError:
        log(usage)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print usage
            sys.exit()

        elif opt in ("-v", "--verbose"):
            verbose = True

        elif opt in ("-f", "--file"):
            file_name = arg

    if file_name == '':
        log("Bad args: {:s}".format(usage))

    else:
        list_file = lintasm.parse(file_name, verbose)

        warnings = list_file.check_jsr()
        for w in warnings:
            log(w)

        warnings = list_file.enforce_size_specifier()
        for w in warnings:
            log(w)

        dead_code = list_file.locate_dead_code()
        for d in dead_code:
            log("Warning 2003: Dead code > {:s}".format(d[0]))

        hot_code = list_file.get_hot_routines()
        log(" ")
        log("Hot Routines")
        log("Label, Count")
        log("------------")
        for h in hot_code:
            log(h)

        list_file.write()

def clean_log():
    with open(FULL_LOG_PATH, "w") as out_file:
        out_file.write("{:s} {:s}\n".format(
            "Lintroller log ::", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))


def log(entry):
    with open(FULL_LOG_PATH, "a") as out_file:
        out_file.write("{:s}\n".format(entry))


# -------------------------------
if __name__ == "__main__":
    main(sys.argv[1:])
