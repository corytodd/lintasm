# Lintasm #

This utility is for linting your CASM-assembled microprocessor code. The primary goal is to quickly check for unsafe code. Secondary warning are also emitted for code that
does not meet best practices such as size-specifiers (e.g. .b, .w, .l) and dead code detection.

### Requirements ###

* Python 2.7
* CASM assembler

### How do I get set up? ###

We use PSPad as our assembly code editor. A PSPad project file is included with this project that shows how to get up and running.

### Contribution guidelines ###

* All submitted code must be accompanied by unit tests
* Squash all commits before creating your pull request

### Who do I talk to? ###

* lint@lucidlobster.com